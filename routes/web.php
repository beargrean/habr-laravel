<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::redirect('/', '/ru/hub/php/');

Route::redirect('/ru/hub/php/all','/ru/hub/php/');


Route::get('/ru/hub/php/{param?}', 'IndexController@index')
    ->name('index');


Route::get('/ru/company/{company_nick}/blog/{post_id}/', 'PostController@getCompanyPost')
    ->name('company_post');

Route::get('/ru/post/{post_id}/', 'PostController@getUserPost')
    ->name('user_post');


Route::patch('/vote/{post_id}/', 'PostController@updatePostRating')
    ->name('vote');


Route::get('/users/{user}', function (App\Models\User $user) {
    return $user;
})->name('user_page');

Route::get('/company/{company}/', function (App\Models\Company $company) {
    return $company;
})->name('company_page');


//Route::get('/webpack', function () {
//    return view('webpack_test');
//});

//Route::get('robots.txt', 'RobotsTxtController');
