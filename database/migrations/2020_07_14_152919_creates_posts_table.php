<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatesPostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('userId');
            $table->integer('companyId')->nullable(true);
            $table->text('title');
            $table->text('contentPreview');
            $table->text('content');
            $table->datetime('publicationDate');
            $table->json('hubs')->nullable(true);
            $table->json('tags')->nullable(true);
            $table->json('marks')->nullable(true);
            $table->json('votes');
            $table->integer('rating')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
//        Schema::table('posts', function (Blueprint $table) {
//            $table->dropColumn('created_at', 'updated_at');
//        });
    }
}
