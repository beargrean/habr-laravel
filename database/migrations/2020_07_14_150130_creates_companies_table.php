<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatesCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies', function (Blueprint $table) {
            $table->increments('id');
            $table->text('title')->nullable(true);
            $table->text('nick');
            $table->text('avatar')->nullable(true);
            $table->text('banner')->nullable(true);
            $table->text('description')->nullable(true);
            $table->text('site')->nullable(true);
            $table->json('contacts')->nullable(true);
            $table->json('informationBlocks');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companies');
    }
}
