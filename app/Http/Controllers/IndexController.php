<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;

class IndexController extends Controller
{
    private const PAGINATE = 2;

    public function index($param = null)
    {
        if(isset($param)) {
            if(stripos($param, 'top') !== false) {

                sscanf($param,"top%d", $ratingLimit);

                if(in_array($ratingLimit, [10, 25, 50, 100])) {

                    $posts = Post::with(['user', 'company'])->where('rating', '>=', $ratingLimit)->paginate(self::PAGINATE);
                }
            }
        }

        if(!isset($posts)) {

            $posts = Post::with(['user', 'company'])->paginate(self::PAGINATE);
        }

        return view('posts_list', [
            'posts' => $posts,
            'ratingLimit' => ($ratingLimit ?? null)
        ]);
    }
}
