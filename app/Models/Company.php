<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/**
 * Class Company
 *
 * @package App\Models
 * @property int $id
 * @property string $title
 * @property string $nick
 * @property string $avatar
 * @property string $banner
 * @property string $description
 * @property string $site
 * @property object $contacts
 * @property object $informationBlocks
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Company newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Company newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Company query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Company whereAvatar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Company whereBanner($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Company whereContacts($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Company whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Company whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Company whereInformationBlocks($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Company whereNick($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Company whereSite($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Company whereTitle($value)
 * @mixin \Eloquent
 */
class Company extends Model
{

    public $timestamps = false;

    protected $casts = [
        'contacts' => 'object',
        'informationBlocks' => 'object'
    ];

    public function getRouteKeyName()
    {
        return 'nick';
    }

}
