@php
    /** @var \App\Models\Post $post */
@endphp

@extends('layouts.main')

@section('page_content')

    <div class="layout__cell layout__cell_body">

        <div class="page-header__banner">
            <a href="{{ $post->company->site ?? '' }}" target="_blank" rel=" noopener"><img src="{{ $post->company->banner ?? '' }}" alt="company_banner"></a>
        </div>

        <div class="page-header page-header_small page-header_bordered" id="company_{{ $post->company->id ?? '' }}">
            <div class="page-header_wrapper">
                <div class="media-obj media-obj_company">
                    <a href="{{ route('company_page', ['company' => $post->company->nick]) }}" class="media-obj__image page-header__image">
                        <img src="{{ $post->company->avatar ?? '' }}" width="48" height="48" class="company-info__image-pic">
                    </a>

                    <div class="media-obj__body media-obj__body_page-header media-obj__body_page-header_branding">
                        <div class="page-header__info">
                            <div class="page-header__info-title">{{ $post->company->title ?? '' }}</div>
                            <div class="page-header__info-desc">{{ $post->company->description ?? '' }}</div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <div class="column-wrapper js-sticky-wrapper">
            <div class="content_left js-content_left">
                <div class="company_post">

                    <article class="post post_full" id="post_{{ $post->id ?? '' }}" lang="ru">
                        <div class="post__wrapper">
                            <header class="post__meta">
                                <a href="{{ route('user_page', ['user' => $post->user->nick]) }}" class="post__user-info user-info" title="Автор публикации">
                                    <img src="{{ $post->user->avatar ?? '' }}" width="24" height="24" class="user-info__image-pic user-info__image-pic_small">
                                    <span class="user-info__nickname user-info__nickname_small">{{ $post->user->nick ?? '' }}</span>
                                </a>

                                <span class="post__time" data-time_published="">{{ $post->publicationDateFormatted ?? '' }}</span>
                            </header>

                            <h1 class="post__title post__title_full">
                                <span class="post__title-text">{{ $post->title ?? '' }}</span>
                            </h1>

                            @if (isset($post->hubs))
                                <ul class="post__hubs post__hubs_full-post inline-list">
                                    @foreach ($post->hubs as $hub)
                                        <li class="inline-list__item inline-list__item_hub">
                                            <a href="{{ $hub->hubLink ?? '' }}" class="inline-list__item-link hub-link " title="Вы не подписаны на этот хаб" onclick="if (typeof ga === 'function') { ga('send', 'event', 'hub', 'post page', '{{ $hub->hubName ?? ''}}'); }">{{ $hub->hubName ?? ''}}</a>
                                            @if (!$loop->last)
                                                ,
                                            @endif
                                        </li>
                                    @endforeach
                                </ul>
                            @endif

                            @if (isset($post->marks))
                                <ul class="post__marks inline-list">
                                    @foreach ($post->marks as $mark)
                                        <li class="inline-list__item inline-list__item_post-type">
                                            @if (isset($mark->markLink))
                                                <a href="{{ $mark->markLink ?? '' }}" class="post__type-label" title="{{ $mark->markTitle ?? '' }}">{{ $mark->markLabel ?? '' }}</a>
                                            @else
                                                <span class="post__type-label" title="{{ $mark->markTitle ?? '' }}">{{ $mark->markLabel ?? '' }}</span>
                                            @endif
                                        </li>
                                    @endforeach
                                </ul>
                            @endif

                            <div class="post__body post__body_full">

                                <div class="post__text post__text-html post__text_v1" id="post-content-body"
                                     data-io-article-url="{{ route('company_post', ['post_id' => $post->id, 'company_nick' => $post->company->nick]) }}">
                                    {!! $post->content ?? ''  !!}
                                </div>

                            </div>

                            @if (isset($post->tags))
                                <dl class="post__tags">
                                    <dt class="post__tags-label">Теги:</dt>
                                    <dd class="post__tags-list">
                                        <ul class="inline-list inline-list_fav-tags js-post-tags">
                                            @foreach ($post->tags as $tag)
                                                <li class="inline-list__item inline-list__item_tag">
                                                    <a href="{{ $tag->tagLink ?? '' }}" rel="tag" class="inline-list__item-link post__tag  ">
                                                        {{ $tag->tagLabel ?? '' }}
                                                    </a>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </dd>
                                </dl>
                            @endif

                            @if (isset($post->hubs))
                                <dl class="post__tags">
                                    <dt class="post__tags-label">Хабы:</dt>
                                    <dd class="post__tags-list">
                                        <ul class="inline-list inline-list_fav-tags js-post-hubs">
                                            @foreach ($post->hubs as $hub)
                                                <li class="inline-list__item inline-list__item_tag">
                                                    <a href="{{ $hub->hubLink ?? '' }}" rel="tag" class="inline-list__item-link post__tag">
                                                        {{ $hub->hubName ?? ''}}
                                                    </a>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </dd>
                                </dl>
                            @endif

                        </div>

                        <div class="overlay hidden" id="js-vote-reason">
                            <div class="popup popup_reasons">
                                <div class="popup__head popup__head_lang-settings">
                                    <span class="popup__head-title">Укажите причину минуса, чтобы автор поработал над ошибками</span>
                                    <button type="button" class="btn btn_small btn_popup-close js-hide_vote-reason">
                                        <svg class="icon-svg" width="12" height="12">
                                            <use xlink:href="https://habr.com/5efdcb8a/images/common-svg-sprite.svg#close"></use>
                                        </svg>
                                    </button>
                                </div>
                                <div class="popup__body">
                               <span class="radio radio_custom reasons-vote__item js-vote-popup_list-item">
                                  <input type="radio" id="reasonList" name="reasonList" class="radio__input js-vote_radio" value="">
                                  <label for="reasonList" class="radio__label radio__label_another js-vote_title"></label>
                                </span>
                                    <div id="js-vote-popup_list"></div>
                                    <button type="button" class="btn btn_blue btn_huge js-vote_send" disabled="">Отправить анонимно</button>
                                </div>
                            </div>
                        </div>

                    </article>

                    <form action="/json/favorites/" method="post" class="form form_bordered form_favorites-tags hidden" id="edit_tags_form">
                        <input type="hidden" name="action" value="add">
                        <input type="hidden" name="ti" value="0">
                        <input type="hidden" name="tt" value="0">

                        <button type="button" class="btn form__close-btn" onclick="closeForm(this)" title="Закрыть"><svg class="icon-svg icon-svg_navbar-close-search" width="31" height="32" viewBox="0 0 31 32" aria-hidden="true" version="1.1" role="img"><path d="M26.67 0L15.217 11.448 3.77 0 0 3.77l11.447 11.45L0 26.666l3.77 3.77L15.218 18.99l11.45 11.448 3.772-3.77-11.448-11.45L30.44 3.772z"></path></svg>
                        </button>

                        <fieldset class="form__fieldset">
                            <legend class="form__legend">Пометьте публикацию своими метками</legend>
                            <input type="text" name="tags_string" class="form__text-field" autocomplete="off">
                            <span class="form__desc">Метки лучше разделять запятой. Например: <i>программирование, алгоритмы</i></span>
                        </fieldset>

                        <div class="form__footer">
                            <button type="submit" class="btn btn_x-large btn_outline_blue" disabled="">Сохранить</button>
                        </div>
                    </form>

                    <div class="post-additionals post-additionals_company">

                        <ul class="post-stats post-stats_post js-user_"
                            data-post-type="publish_ugc_ru,h_91,h_260,h_18812,f_develop" id="infopanel_post_508670">
                            <li class="post-stats__item post-stats__item_voting-wjt">
                                <div class="voting-wjt voting-wjt_post js-post-vote" data-id="508670" data-type="2">
                                    <button type="button" id="vote_plus" class="btn" data-action="plus" onclick="posts_vote_plus(this)" title="Голосовать" {{-- disabled="" --}}>
                                <span class="icon-svg_arrow-up" style="font-size: 22px; padding: 5px;  color: #9dd1e0">
                                    &#11014;
                                </span>
                                    </button>

                                    <span id="vote_result" class="voting-wjt__counter  voting-wjt__counter_positive  js-score" onclick="{{-- posts_vote_result(8, 7, 1) --}}"
                                          title="Всего голосов {{ $post->votes->plusVotes + $post->votes->minusVotes }}: ↑{{ $post->votes->plusVotes }} и ↓{{ $post->votes->minusVotes }}">
                                @if ($post->rating > 0)
                                            +{{ $post->rating }}
                                        @else
                                            {{ $post->rating }}
                                        @endif
                            </span>

                                    <button type="button" id="vote_minus" class="btn" data-action="minus" onclick="posts_vote_minus(this);" title="Голосовать"  {{-- disabled="" --}}>
                                <span class="icon-svg_arrow-down" style="font-size: 22px; padding: 5px; color: #9dd1e0; transform: none;">
                                    &#11015;
                                </span>
                                    </button>
                                </div>
                            </li>

                            <li class="post-stats__item post-stats__item_bookmark">
                                <button type="button" class="btn bookmark-btn bookmark-btn_post "
                                        data-post-type="publish_ugc_ru,h_91,h_260,h_18812,f_develop" data-type="2"
                                        data-id="508670" data-action="add" title="Добавить в закладки"
                                        onclick="posts_add_to_favorite(this);">
                            <span class="btn_inner">
                                <svg class="icon-svg_bookmark" width="10" height="16">
                                    <use xlink:href="https://habr.com/5efb62bc/images/common-svg-sprite.svg#book"></use>
                                </svg>
                                <span
                                    class="bookmark__counter js-favs_count"
                                    title="Количество пользователей, добавивших публикацию в закладки">
                                    0</span>
                            </span>
                                </button>
                            </li>

                            <li class="post-stats__item post-stats__item_views">
                                <div class="post-stats__views" title="Количество просмотров">
                                    <svg class="icon-svg_views-count" width="21" height="12">
                                        <use xlink:href="https://habr.com/5efb62bc/images/common-svg-sprite.svg#eye"></use>
                                    </svg>
                                    <span class="post-stats__views-count">
                                0k</span>
                                </div>
                            </li>

                            <li class="post-stats__item post-stats__item_comments">
                                <a href="#" class="post-stats__comments-link"
                                   rel="nofollow">
                                    <svg class="icon-svg_post-comments" width="16" height="16">
                                        <use xlink:href="https://habr.com/5efb62bc/images/common-svg-sprite.svg#comment"></use>
                                    </svg>
                                    <span class="post-stats__comments-text" title="Добавить комментарий">Комментировать</span>
                                </a>
                            </li>

                            <li class="post-stats__item post-stats__item_share">
                                <div class="dropdown dropdown_share">
                                    <div href="https://habr.com/ru/post/508670/#comments" data-toggle="dropdown"
                                         class="post-stats__share" rel="nofollow" tabindex="0">
                                        <svg class="icon-svg_post-share" width="24" height="24">
                                            <use xlink:href="https://habr.com/5efb62bc/images/common-svg-sprite.svg#share">
                                            </use>
                                        </svg>
                                        <span class="post-stats__comments-text" title="Поделиться">
                                  Поделиться
                                </span>
                                    </div>

                                    <div class="dropdown-container">
                                        <div class="post-share">
                                            <ul class="post-share__buttons">
                                                <li class="post-share__item post-share__item_post">
                                            <span class="post-share__item-link post-share__item-link_normal post-share__item-link_copy"
                                                  title="Скопировать ссылку" onclick="copyCurrentUrl(), $('.dropdown_share').removeClass('dropdown_active')">
                                            Скопировать ссылку
                                            </span>
                                                </li>
                                                <li class="post-share__item post-share__item_post">
                                                    <a href="#"
                                                       class="post-share__item-link post-share__item-link_normal post-share__item-link_facebook"
                                                       title="Facebook"
                                                       onclick="window.open(this.href, 'Facebook', 'width=640,height=436,toolbar=0,status=0'); return false">
                                                        Facebook
                                                    </a>
                                                </li>
                                                <li class="post-share__item post-share__item_post">
                                                    <a href="#"
                                                       class="post-share__item-link post-share__item-link_normal post-share__item-link_twitter"
                                                       title="Twitter"
                                                       onclick="window.open(this.href, 'Twitter', 'width=800,height=300,resizable=yes,toolbar=0,status=0'); return false">
                                                        Twitter
                                                    </a>
                                                </li>
                                                <li class="post-share__item post-share__item_post">
                                                    <a href="#
                                               class="post-share__item-link post-share__item-link_normal post-share__item-link_vkontakte"
                                                    title="ВКонтакте"
                                                    onclick="window.open(this.href, 'ВКонтакте', 'width=800,height=300,toolbar=0,status=0'); return false">
                                                    ВКонтакте
                                                    </a>
                                                </li>
                                                <li class="post-share__item post-share__item_post">
                                                    <a href="#"
                                                       class="post-share__item-link post-share__item-link_normal post-share__item-link_telegram"
                                                       title="Telegram"
                                                       onclick="window.open(this.href, 'Telegram', 'width=800,height=300,toolbar=0,status=0'); return false">
                                                        Telegram
                                                    </a>
                                                </li>
                                                <li class="post-share__item post-share__item_post">
                                                    <a href="#"
                                                       class="post-share__item-link post-share__item-link_normal post-share__item-link_pocket"
                                                       title="Pocket" target="_blank" rel=" noopener">
                                                        Pocket
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>

                                    </div>
                                </div>
                            </li>
                        </ul>

                        <form action="/json/complaints/add/" class="form form_bordered form_abuse hidden" method="post" id="abuse_form">
                            <input type="hidden" name="tt" value="2">
                            <input type="hidden" name="ti" value="508940">

                            <button type="button" class="btn form__close-btn" onclick="closeForm(this)"><svg class="icon-svg icon-svg_navbar-close-search" width="31" height="32" viewBox="0 0 31 32" aria-hidden="true" version="1.1" role="img"><path d="M26.67 0L15.217 11.448 3.77 0 0 3.77l11.447 11.45L0 26.666l3.77 3.77L15.218 18.99l11.45 11.448 3.772-3.77-11.448-11.45L30.44 3.772z"></path></svg>
                            </button>

                            <fieldset class="form__fieldset">
                                <legend class="form__legend">Нарушение</legend>
                                <input type="text" name="text" class="form__text-field">
                                <span class="form__desc">Опишите суть нарушения</span>
                            </fieldset>

                            <div class="form__footer">
                                <button type="submit" class="btn btn_x-large btn_outline_blue" disabled="">Отправить</button>
                            </div>
                        </form>

                        <form action="/conversations/galimova_ruvds/" method="GET" class="form form_bordered form_admin-causes hidden" id="admin_causes">

                            <input type="hidden" name="moderation" value="508940">

                            <button type="button" class="btn form__close-btn" onclick="closeForm(this)"><svg class="icon-svg icon-svg_navbar-close-search" width="31" height="32" viewBox="0 0 31 32" aria-hidden="true" version="1.1" role="img"><path d="M26.67 0L15.217 11.448 3.77 0 0 3.77l11.447 11.45L0 26.666l3.77 3.77L15.218 18.99l11.45 11.448 3.772-3.77-11.448-11.45L30.44 3.772z"></path></svg>
                            </button>

                            <fieldset class="form__fieldset form__fieldset_admin-causes">
                                <legend class="form__legend">Выберите рекомендации для отправки автору:</legend>

                                <div class="checkbox-group_columns">
        <span class="checkbox checkbox_custom checkbox_recommendation-form">
          <input type="checkbox" class="form__input form__input_checkbox" name="id[]" id="checkbox1" value="1">
          <label for="checkbox1" class="checkbox__label">Указан только блог</label>
        </span>

                                    <span class="checkbox checkbox_custom checkbox_recommendation-form">
          <input type="checkbox" class="form__input form__input_checkbox" name="id[]" id="checkbox2" value="2">
          <label for="checkbox2" class="checkbox__label">Орфографические ошибки</label>
        </span>

                                    <span class="checkbox checkbox_custom checkbox_recommendation-form">
          <input type="checkbox" class="form__input form__input_checkbox" name="id[]" id="checkbox3" value="3">
          <label for="checkbox3" class="checkbox__label">Пунктуационные ошибки</label>
        </span>

                                    <span class="checkbox checkbox_custom checkbox_recommendation-form">
          <input type="checkbox" class="form__input form__input_checkbox" name="id[]" id="checkbox4" value="4">
          <label for="checkbox4" class="checkbox__label">Отступы</label>
        </span>

                                    <span class="checkbox checkbox_custom checkbox_recommendation-form">
          <input type="checkbox" class="form__input form__input_checkbox" name="id[]" id="checkbox5" value="5">
          <label for="checkbox5" class="checkbox__label">Текст-простыня</label>
        </span>

                                    <span class="checkbox checkbox_custom checkbox_recommendation-form">
          <input type="checkbox" class="form__input form__input_checkbox" name="id[]" id="checkbox6" value="6">
          <label for="checkbox6" class="checkbox__label">Короткие предложения</label>
        </span>
                                    <span class="checkbox checkbox_custom checkbox_recommendation-form">
          <input type="checkbox" class="form__input form__input_checkbox" name="id[]" id="checkbox7" value="7">
          <label for="checkbox7" class="checkbox__label">Смайлики</label>
        </span>
                                    <span class="checkbox checkbox_custom checkbox_recommendation-form">
          <input type="checkbox" class="form__input form__input_checkbox" name="id[]" id="checkbox8" value="8">
          <label for="checkbox8" class="checkbox__label">Много форматирования</label>
        </span>
                                    <span class="checkbox checkbox_custom checkbox_recommendation-form">
          <input type="checkbox" class="form__input form__input_checkbox" name="id[]" id="checkbox9" value="9">
          <label for="checkbox9" class="checkbox__label">Картинки</label>
        </span>
                                    <span class="checkbox checkbox_custom checkbox_recommendation-form">
          <input type="checkbox" class="form__input form__input_checkbox" name="id[]" id="checkbox10" value="10">
          <label for="checkbox10" class="checkbox__label">Ссылки</label>
        </span>
                                    <span class="checkbox checkbox_custom checkbox_recommendation-form">
          <input type="checkbox" class="form__input form__input_checkbox" name="id[]" id="checkbox11" value="11">
          <label for="checkbox11" class="checkbox__label">Оформление кода</label>
        </span>
                                    <span class="checkbox checkbox_custom checkbox_recommendation-form">
          <input type="checkbox" class="form__input form__input_checkbox" name="id[]" id="checkbox12" value="12">
          <label for="checkbox12" class="checkbox__label">Рекламный характер</label>
        </span>
                                </div>
                            </fieldset>

                            <div class="form__footer">
                                <button type="submit" class="btn btn_x-large btn_outline_blue" disabled="">Отправить</button>
                            </div>
                        </form>

                        <div class="company-info company-info_post-additional author-panel">

                            <div class="author-panel__user-info">
                                <div class="user-info" rel="user-popover" data-user-login="{{ $post->user->nick ?? '' }}">
                                    <a href="{{ route('user_page', ['user' => $post->user->nick]) }}" class="media-obj__image"
                                       onclick="if (typeof ga === 'function') { ga('send', 'event', 'author_info_top', 'profile', '{{ $post->user->nick ?? '' }}');}">
                                        <img src="{{ $post->user->avatar ?? '' }}" width="48" height="48" class="media-obj__image-pic media-obj__image-pic_user">
                                    </a>

                                    <div class="user-info__about">
                                        <div class="user-info__links">
                                            <a href="{{ route('user_page', ['user' => $post->user->nick]) }}" class="user-info__fullname"
                                               onclick="if (typeof ga === 'function') { ga('send', 'event', 'author_info_top', 'profile', '{{ $post->user->nick ?? '' }}');}">{{ $post->user->name ?? '' }}</a>&nbsp;
                                            <a href="{{ route('user_page', ['user' => $post->user->nick]) }}" class="user-info__nickname user-info__nickname_doggy"
                                               onclick="if (typeof ga === 'function') { ga('send', 'event', 'author_info_top', 'profile', '{{ $post->user->nick ?? '' }}');}">{{ $post->user->nick ?? '' }}</a>
                                        </div>

                                        <div class="user-info__specialization">{{ $post->user->specialization ?? '' }}</div>
                                    </div>
                                </div>

                                <div class="overlay hidden" id="js-donate">
                                    <div class="popup">
                                        <div class="popup__head popup__head_lang-settings">
                                    <span class="popup__head-title js-popup_title"
                                          data-section="1">Платежная система</span>
                                            <button type="button" class="btn btn_small btn_popup-close js-hide-donate">
                                                <svg class="icon-svg" width="12" height="12">
                                                    <use
                                                        xlink:href="https://habr.com/5efb62bc/images/common-svg-sprite.svg#close"></use>
                                                </svg>
                                            </button>
                                        </div>
                                        <div class="popup__body js-donate-popup_body">
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="company-info__about">
                                <div class="media-obj media-obj_company">
                                    <a href="{{ route('company_page', ['company' => $post->company->nick]) }}" class="media-obj__image page-header__image">
                                        <img src="{{ $post->company->avatar ?? '' }}" width="48" height="48" class="company-info__image-pic">
                                    </a>

                                    <div class="media-obj__body media-obj__body_page-header media-obj__body_page-header_branding">

                                        <div class="page-header__info">
                                            <div class="page-header__info-title">{{ $post->company->title ?? '' }}</div>
                                            <div class="page-header__info-desc">{{ $post->company->description ?? '' }}</div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            @if (isset($post->company->contacts))
                                <ul class="company-info__contacts inline-list">
                                    @foreach($post->company->contacts as $contact)
                                        <li class="inline-list__item inline-list__item_contact-link"><a href="{{ $contact->link ?? '' }}">{{ $contact->title ?? '' }}</a></li>
                                    @endforeach
                                </ul>
                            @endif

                        </div>

                    </div>

                    <div class="default-block default-block_content">
                        <div class="default-block__header default-block__header_large">
                            <h2 class="default-block__header-title default-block__header-title_large">Похожие публикации</h2>
                        </div>
                        <div class="default-block__content">
                            <ul class="content-list">
                                <li class="content-list__item content-list__item_devided post-info">
                                    <span class="post-info__date">28 декабря 2017 в 14:20</span>
                                    <h3 class="post-info__title post-info__title_large">
                                        <a href="https://habr.com/ru/post/345786/" class="post-info__title post-info__title_large" onclick="if (typeof ga === 'function') { ga('send', 'event', 'tm_block', 'similar_posts', 'common', '1'); }">
                                            Блог RuVDS. Дайджест постов и наши итоги года
                                        </a>
                                    </h3>

                                    <div class="post-info__meta">
                                    <span class="post-info__meta-item" title="Рейтинг">
                                      <svg class="post-info__meta-icon icon-svg_rating" width="11" height="13"><use xlink:href="https://habr.com/5efdcb8a/images/common-svg-sprite.svg#arrow-bold"></use></svg>
                                          <span class="post-info__meta-counter">0</span>
                                    </span>
                                        <span class="post-info__meta-item" title="Количество просмотров">
                                      <svg class="post-info__meta-icon icon-svg_views" width="16" height="9"><use xlink:href="https://habr.com/5efdcb8a/images/common-svg-sprite.svg#eye"></use></svg><span class="post-info__meta-counter">0k</span>
                                    </span>
                                        <span class="post-info__meta-item" title="Закладки">
                                      <svg class="post-info__meta-icon icon-svg_bookmark-mini" width="8" height="13"><use xlink:href="https://habr.com/5efdcb8a/images/common-svg-sprite.svg#book"></use></svg><span class="post-info__meta-counter">0</span>
                                    </span>
                                        <a href="#" class="post-info__meta-item" rel="nofollow" title="Комментарии">
                                            <svg class="post-info__meta-icon icon-svg_comments" width="14" height="13"><use xlink:href="https://habr.com/5efdcb8a/images/common-svg-sprite.svg#comment"></use></svg><span class="post-info__meta-counter">0</span>
                                        </a>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div class="comments-section" id="comments">
                        <header class="comments-section__head">
                            <h2 class="comments-section__head-title">
                                Комментарии
                                <span class="comments-section__head-counter" id="comments_count">
                          0
                        </span>
                            </h2>
                        </header>

                        <ul class="content-list content-list_comments" id="comments-list">

                        </ul>

                        <div class="js-form_placeholder">
                            <p class="for_users_only_msg">Только&nbsp;<a href="#">полноправные пользователи</a> могут оставлять комментарии. <a href="#">Войдите</a>, пожалуйста.</p>
                        </div>
                    </div>

                </div>
            </div>

            <div class="sidebar">

                <div class="sidebar_right_ad">

                </div>

                @if(isset($post->company->informationBlocks))
                    <div class="sidebar_right sidebar_content-area sticky_init">
                        @foreach($post->company->informationBlocks as $block)

                            @switch($block->type)

                                @case('information')
                                <div class="default-block default-block_sidebar">
                                    <div class="default-block__header">
                                        <h2 class="default-block__header-title">{{ $block->title ?? '' }}</h2>
                                    </div>
                                    <div class="default-block__content default-block__content_profile-summary">
                                        <ul class="defination-list">

                                            @foreach($block->items as $item)
                                                <li class="defination-list__item defination-list__item_profile-summary">
                                                    <h3 class="defination-list__label defination-list__label_profile-summary">{{ $item->label ?? '' }}</h3>
                                                    <span class="defination-list__value">

                                                    @if(isset($item->site))
                                                            <a href="{{ $item->site ?? '' }}" target="_blank" class="defination-list__link" title="{{ $item->site ?? '' }}" rel=" noopener">{!! $item->value ?? '' !!}</a>
                                                        @elseif(isset($item->link))
                                                            <a title="{{ $item->value ?? '' }}" href="{{ $item->link ?? '' }}">{!! $item->value ?? '' !!}</a>
                                                        @else
                                                            {!! $item->value ?? '' !!}
                                                        @endif

                                                    </span>
                                                </li>
                                            @endforeach

                                        </ul>
                                    </div>
                                </div>
                                @break

                                @case('links')
                                <div class="default-block default-block_sidebar">
                                    <div class="default-block__header">
                                        <h2 class="default-block__header-title">{{ $block->title ?? '' }}</h2>
                                    </div>
                                    <div class="default-block__content">
                                        <ul class="content-list content-list_company-links">

                                            @foreach($block->items as $item)
                                                <li class="content-list__item content-list__item_company-links company-links">
                                                    <a href="{{ $item->link }}" class="company-links__title">{{ $item->title }}</a>
                                                    <div class="company-links__link">{{ $item->source }}</div>
                                                </li>
                                            @endforeach

                                        </ul>
                                    </div>
                                </div>
                                @break

                                @case('applications')
                                <div class="default-block default-block_sidebar">
                                    <div class="default-block__header">
                                        <h2 class="default-block__header-title">{{ $block->title ?? '' }}</h2>
                                    </div>
                                    <div class="default-block__content">
                                        <ul class="content-list content-list_company-wjt" id="company-wjt">
                                            @foreach($block->items as $item)
                                                <li class="content-list__item content-list__item_company-wjt ">
                                                    <div class="media-obj company-app">
                                                        <div class="media-obj__image media-obj__image_company-wjt">
                                                            <img src="{{ $item->image }}" class="company-app__image" width="48" height="48">
                                                        </div>
                                                        <div class="media-obj__body media-obj__body_company-wjt">
                                                            <h3 class="company-app__title">{{ $item->title }}</h3>
                                                            <div class="company-app__description">{{ $item->description }}</div>
                                                        </div>
                                                    </div>
                                                    <ul class="app-links">
                                                        @foreach($item->links as $link)
                                                            <li class="app-links__item">
                                                                <a href="{{ $link->link }}" class="app-links__item-link">
                                                                    <span class="app-links__item-name">{{ $link->title }}</span>
                                                                </a>
                                                            </li>
                                                        @endforeach
                                                    </ul>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                                @break

                                @case('vidget')
                                <div class="default-block default-block_sidebar">
                                    <div class="default-block__header">
                                        <h2 class="default-block__header-title">{{ $block->title }}</h2>
                                    </div>
                                    <div class="default-block__content  sidebar-block">
                                        <div class="sidebar-block__banner">
                                            <a href="{{ $block->bannerLink }}" class="sidebar-block__banner-link">
                                                <img src="{{ $block->bannerImg }}" class="sidebar-block__banner-image">
                                            </a>
                                        </div>
                                        <div class="sidebar-block__paragraph"></div>
                                    </div>
                                </div>
                                @break

                                @case('blog')
                                <div class="default-block">
                                    <div class="default-block__header">
                                        <h2 class="default-block__header-title">{{ $block->title }}</h2>
                                    </div>
                                    <div class="default-block__content">
                                        <ul class="content-list">
                                            @foreach($block->posts as $post)
                                                <li class="content-list__item content-list__item_devided post-info">
                                                    <h3 class="post-info__title">
                                                        <a href="{{ $post->link }}" class="post-info__title">{{ $post->title }}</a>
                                                    </h3>
                                                    <div class="post-info__meta">
                                                        <span class="post-info__meta-item" title="Количество просмотров">
                                                            <svg class="post-info__meta-icon icon-svg_views" width="16" height="9"><use xlink:href="https://habr.com/5efdcb8a/images/common-svg-sprite.svg#eye"></use></svg>
                                                            <span class="post-info__meta-counter post-info__meta-counter_small">{{ $post->views }}</span>
                                                        </span>
                                                        <a href="#" class="post-info__meta-item" rel="nofollow" title="Комментарии">
                                                            <svg class="post-info__meta-icon icon-svg_comments" width="14" height="13"><use xlink:href="https://habr.com/5efdcb8a/images/common-svg-sprite.svg#comment"></use></svg>
                                                            <span class="post-info__meta-counter post-info__meta-counter_small">{{ $post->comments }}</span>
                                                        </a>
                                                    </div>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                                @break

                            @endswitch
                        @endforeach
                    </div>
                @endif

            </div>

        </div>

    </div>

@endsection
