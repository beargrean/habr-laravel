<?php
/** @var App\Models\Post[] $posts */
?>

@extends('layouts.main')

@section('page_content')
    <div class="layout__cell layout__cell_body">

        <div class="page-header page-header_full" id="hub_260">
            <div class="page-header_wrapper">
                <div class="media-obj media-obj_page-header">
                    <a href="#" class="media-obj__image">
                        <img src="//habrastorage.org/getpro/habr/hub/98a/7a8/831/98a7a88319d5644cdc627b5e04b47d0f.png" width="48" height="48" class="media-obj__image-pic">
                    </a>

                    <div class="media-obj__body media-obj__body_page-header media-obj__body_page-header_hub">
                        <div class="page-header__stats">
                            <div class="page-header__stats-value">0,0</div>
                            <div class="page-header__stats-label">Рейтинг</div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="page-header__info">
                <h1 class="page-header__info-title">PHP</h1>
                <span class="n-profiled_hub" title="Профильный хаб"></span>
                <h2 class="page-header__info-desc">
                    Скриптовый язык общего назначения
                </h2>
            </div>
        </div>

        <div class="column-wrapper column-wrapper_tabs js-sticky-wrapper">
            <div class="content_left js-content_left">
                <div class="tabs">
                    <div class="tabs__level tabs-level_top tabs-menu">
                        <a href="{{ route('index') }}" class="tabs-menu__item tabs-menu__item_link" rel="nofollow">
                            <h3 class="tabs-menu__item-text tabs-menu__item-text_active">
                                Все подряд
                            </h3>
                        </a>
                        <a href="#" class="tabs-menu__item tabs-menu__item_link" rel="nofollow">
                            <h3 class="tabs-menu__item-text">
                                Лучшие
                            </h3>
                        </a>
                        <a href="#" class="tabs-menu__item tabs-menu__item_link" rel="nofollow">
                            <h3 class="tabs-menu__item-text ">
                                Авторы
                            </h3>
                        </a>
                    </div>

                    <div class="tabs__level tabs__level_bottom">
                        <ul class="toggle-menu ">
                            <li class="toggle-menu__item">
                                <a href="{{ route('index', ['param' => 'all']) }}" class="toggle-menu__item-link {{ isset($ratingLimit) ? '' : 'toggle-menu__item-link_active' }}" rel="nofollow" title="Все публикации в хронологическом порядке">
                                    Без порога

                                </a>
                            </li>
                            <li class="toggle-menu__item">
                                <a href="{{ route('index', ['param' => 'top10']) }}" class="toggle-menu__item-link {{ isset($ratingLimit) && $ratingLimit == 10 ? 'toggle-menu__item-link_active' : '' }}" rel="nofollow" title="Все публикации с рейтингом 10 и выше">
                                    ≥10


                                </a>
                            </li>
                            <li class="toggle-menu__item">
                                <a href="{{ route('index', ['param' => 'top25']) }}" class="toggle-menu__item-link {{ isset($ratingLimit) && $ratingLimit == 25 ? 'toggle-menu__item-link_active' : '' }}" rel="nofollow" title="Все публикации с рейтингом 25 и выше">
                                    ≥25


                                </a>
                            </li>
                            <li class="toggle-menu__item">
                                <a href="{{ route('index', ['param' => 'top50']) }}" class="toggle-menu__item-link {{ isset($ratingLimit) && $ratingLimit == 50 ? 'toggle-menu__item-link_active' : '' }}" rel="nofollow" title="Все публикации с рейтингом 50 и выше">
                                    ≥50


                                </a>
                            </li>
                            <li class="toggle-menu__item">
                                <a href="{{ route('index', ['param' => 'top100']) }}" class="toggle-menu__item-link {{ isset($ratingLimit) && $ratingLimit == 100 ? 'toggle-menu__item-link_active' : '' }}" rel="nofollow" title="Все публикации с рейтингом 100 и выше">
                                    ≥100


                                </a>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="posts_list">
                    <ul class="content-list shortcuts_items">

                        @foreach ($posts as $post)

                            @if(isset($post))
                                @include('layouts.post_preview', ['post' => $post])
                            @endif

                        @endforeach

                    </ul>

                    <div class="page__footer">

                        {{ $posts->links('layouts.pagination') }}

                    </div>

                </div>
            </div>

            <div class="sidebar">
                <div class="sidebar_right_ad">

                </div>
                <div class="sidebar_right js-sidebar_right sidebar_content-area sticky_init">

                    <div class="default-block default-block_sidebar">
                        <div class="default-block__header">
                            <h3 class="default-block__header-title">Спонсоры сообщества</h3>
                        </div>
                        <div class="default-block__content">
                            <ul class="content-list content-list_partners-block">
                                <li class="content-list__item content-list__item_partners-block">
                                    <a href="https://u.tmtm.ru/vtb_sb_2020" rel="nofollow" class="partner-info">
                                        <div class="partner-info__head">
                                            <span class="partner-info__title">ВТБ</span>
                                            <img src="https://habrastorage.org/getpro/tmtm/pictures/dce/bfe/ca6/dcebfeca67166611a3fe63b567bb1a52.png" class="partner-info__image">
                                        </div>

                                        <div class="partner-info__description">
                                            Развивает и делает финансовые услуги доступными онлайн.
                                        </div>
                                    </a>
                                </li>
                                <li class="content-list__item content-list__item_partners-block">
                                    <a href="https://u.tmtm.ru/nspk_sb" rel="nofollow" class="partner-info">
                                        <div class="partner-info__head">
                                            <span class="partner-info__title">НСПК</span>
                                            <img src="https://habrastorage.org/webt/7u/sc/jx/7uscjxtfvndxiizfbs8ei8zu3wq.png" class="partner-info__image">
                                        </div>

                                        <div class="partner-info__description">
                                            Национальная система платежных карт. Настоящий highload, обеспечивает все наши платежи.
                                        </div>
                                    </a>
                                </li>
                                <li class="content-list__item content-list__item_partners-block">
                                    <a href="https://u.tmtm.ru/s-alfa-bttl" rel="nofollow" class="partner-info">
                                        <div class="partner-info__head">
                                            <span class="partner-info__title">Alfa Battle</span>
                                            <img src="https://habrastorage.org/getpro/tmtm/pictures/36e/316/b43/36e316b435d9992a4d1bda4e783b77e7.jpg" class="partner-info__image">
                                        </div>

                                        <div class="partner-info__description">
                                            Чемпионат для Java-разработчиков. Тут решают реальные задачи финтеха.
                                        </div>
                                    </a>
                                </li>
                            </ul>
                        </div>

                        <div class="default-block__footer">
                            <a href="#" class="default-block__footer_link">Как стать спонсором?</a>
                        </div>
                    </div>

                    <div class="default-block default-block_sidebar">
                        <div class="default-block__header">
                            <h3 class="default-block__header-title">Вклад авторов</h3>
                        </div>
                        <div class="default-block__content">
                            <ul class="content-list сontent-list_top-users">
                                <li class="content-list__item content-list__item_top-users">
                                    <a href="#" class="media-obj media-obj_link">
                                        <div class="media-obj__image">
                                            <img src="//habrastorage.org/getpro/habr/avatars/27f/b99/eb3/27fb99eb3e30e3221c5839e88be02395.jpg" class="user-pic__img" width="30" height="30">
                                        </div>

                                        <div class="media-obj__body">
                                            <div class="rating-info rating-info_top-1">
                                                <span class="rating-info__title">pronskiy</span>
                                                <span class="rating-info__stat">8&nbsp;462,4</span>
                                            </div>
                                            <div class="rating-info__progress">
                                                <div class="rating-info__progress-scale" style="width:100%;"></div>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <li class="content-list__item content-list__item_top-users">
                                    <a href="#" class="media-obj media-obj_link">
                                        <div class="media-obj__image">
                                            <img src="//habrastorage.org/getpro/habr/avatars/741/45e/bea/74145ebeab7f222cce402aed2683f9d7.png" class="user-pic__img" width="30" height="30">
                                        </div>

                                        <div class="media-obj__body">
                                            <div class="rating-info rating-info_top-2">
                                                <span class="rating-info__title">AloneCoder</span>
                                                <span class="rating-info__stat">1&nbsp;774,6</span>
                                            </div>
                                            <div class="rating-info__progress">
                                                <div class="rating-info__progress-scale" style="width:21%;"></div>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <li class="content-list__item content-list__item_top-users">
                                    <a href="#" class="media-obj media-obj_link">
                                        <div class="media-obj__image">
                                            <img src="//habrastorage.org/getpro/habr/avatars/2df/fce/7ee/2dffce7ee42c7b79cce513e382cec05c.jpg" class="user-pic__img" width="30" height="30">
                                        </div>

                                        <div class="media-obj__body">
                                            <div class="rating-info rating-info_top-3">
                                                <span class="rating-info__title">SamDark</span>
                                                <span class="rating-info__stat">933,0</span>
                                            </div>
                                            <div class="rating-info__progress">
                                                <div class="rating-info__progress-scale" style="width:11%;"></div>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <li class="content-list__item content-list__item_top-users">
                                    <a href="#" class="media-obj media-obj_link">
                                        <div class="media-obj__image">
                                            <img src="//habrastorage.org/getpro/habr/avatars/c94/ce9/9e3/c94ce99e339f03c02a78c4130bff9eaf.jpg" class="user-pic__img" width="30" height="30">
                                        </div>

                                        <div class="media-obj__body">
                                            <div class="rating-info rating-info_top-4">
                                                <span class="rating-info__title">AntonShevchuk</span>
                                                <span class="rating-info__stat">815,0</span>
                                            </div>
                                            <div class="rating-info__progress">
                                                <div class="rating-info__progress-scale" style="width:10%;"></div>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <li class="content-list__item content-list__item_top-users">
                                    <a href="#" class="media-obj media-obj_link">
                                        <div class="media-obj__image">
                                            <img src="//habrastorage.org/getpro/habr/avatars/371/51f/213/37151f213dce4543faf649cbb6522063.jpg" class="user-pic__img" width="30" height="30">
                                        </div>

                                        <div class="media-obj__body">
                                            <div class="rating-info rating-info_top-5">
                                                <span class="rating-info__title">youROCK</span>
                                                <span class="rating-info__stat">666,0</span>
                                            </div>
                                            <div class="rating-info__progress">
                                                <div class="rating-info__progress-scale" style="width:8%;"></div>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <li class="content-list__item content-list__item_top-users">
                                    <a href="#" class="media-obj media-obj_link">
                                        <div class="media-obj__image">
                                            <img src="//habrastorage.org/getpro/habr/avatars/7cb/450/197/7cb450197c546e0f13d38c61021ac846.jpg" class="user-pic__img" width="30" height="30">
                                        </div>

                                        <div class="media-obj__body">
                                            <div class="rating-info rating-info_top-6">
                                                <span class="rating-info__title">pmurzakov</span>
                                                <span class="rating-info__stat">566,4</span>
                                            </div>
                                            <div class="rating-info__progress">
                                                <div class="rating-info__progress-scale" style="width:7%;"></div>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <li class="content-list__item content-list__item_top-users">
                                    <a href="#" class="media-obj media-obj_link">
                                        <div class="media-obj__image">
                                            <img src="//habr.com/images/stub-user-middle.gif" class="user-pic__img" width="30" height="30">
                                        </div>

                                        <div class="media-obj__body">
                                            <div class="rating-info rating-info_top-7">
                                                <span class="rating-info__title">zapimir</span>
                                                <span class="rating-info__stat">559,0</span>
                                            </div>
                                            <div class="rating-info__progress">
                                                <div class="rating-info__progress-scale" style="width:7%;"></div>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <li class="content-list__item content-list__item_top-users">
                                    <a href="#" class="media-obj media-obj_link">
                                        <div class="media-obj__image">
                                            <img src="//habrastorage.org/getpro/habr/avatars/d2a/e8a/5ac/d2ae8a5ac9876f838ebdd45f3ea3bc3b.png" class="user-pic__img" width="30" height="30">
                                        </div>

                                        <div class="media-obj__body">
                                            <div class="rating-info rating-info_top-8">
                                                <span class="rating-info__title">alexzfort</span>
                                                <span class="rating-info__stat">558,0</span>
                                            </div>
                                            <div class="rating-info__progress">
                                                <div class="rating-info__progress-scale" style="width:7%;"></div>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <li class="content-list__item content-list__item_top-users">
                                    <a href="#" class="media-obj media-obj_link">
                                        <div class="media-obj__image">
                                            <img src="//habrastorage.org/getpro/habr/avatars/fdb/8a5/6e3/fdb8a56e3bbf010b012a4eb597cdf134.jpg" class="user-pic__img" width="30" height="30">
                                        </div>

                                        <div class="media-obj__body">
                                            <div class="rating-info rating-info_top-9">
                                                <span class="rating-info__title">olegbunin</span>
                                                <span class="rating-info__stat">557,0</span>
                                            </div>
                                            <div class="rating-info__progress">
                                                <div class="rating-info__progress-scale" style="width:7%;"></div>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <li class="content-list__item content-list__item_top-users">
                                    <a href="#" class="media-obj media-obj_link">
                                        <div class="media-obj__image">
                                            <img src="//habrastorage.org/getpro/habr/avatars/398/182/212/39818221233f641a20d067615a7e0e01.jpg" class="user-pic__img" width="30" height="30">
                                        </div>

                                        <div class="media-obj__body">
                                            <div class="rating-info rating-info_top-10">
                                                <span class="rating-info__title">iGusev</span>
                                                <span class="rating-info__stat">483,0</span>
                                            </div>
                                            <div class="rating-info__progress">
                                                <div class="rating-info__progress-scale" style="width:6%;"></div>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="default-block__footer">
                            <a href="#" class="default-block__footer-link" rel="nofollow">100 лучших авторов</a>
                        </div>
                    </div>

                    <div class="default-block default-block_sidebar" id="neuro-habr">
                        <div class="default-block">
                            <div class="default-block__header">
                                <h3 class="default-block__header-title">Читают сейчас</h3>
                            </div>

                            <div class="default-block__content default-block__content_most-read" id="broadcast_most-read">
                                <ul class="content-list content-list_most-read">
                                    <li class="content-list__item content-list__item_devided post-info">
                                        <a href="#" class="post-info__title" onclick="if (typeof ga === 'function') { ga('send', 'event', 'tm_block', 'most_read', 'sidebar', '1'); }">Пора обновить ваш монитор</a>
                                        <div class="post-info__meta">
                  <span class="post-info__meta-item">
                    <svg class="post-info__meta-icon icon-svg_views" width="16" height="9"><use xlink:href="https://habr.com/5efdcb8a/images/common-svg-sprite.svg#eye"></use></svg><span class="post-info__meta-counter post-info__meta-counter_small">58,3k</span>
                  </span>
                                            <a href="#" rel="nofollow">
                                                <svg class="post-info__meta-icon icon-svg_comments" width="14" height="13"><use xlink:href="https://habr.com/5efdcb8a/images/common-svg-sprite.svg#comment"></use></svg><span class="post-info__meta-counter post-info__meta-counter_small">626</span>
                                            </a>
                                        </div>
                                    </li>
                                    <li class="content-list__item content-list__item_devided post-info">
                                        <a href="#" class="post-info__title" onclick="if (typeof ga === 'function') { ga('send', 'event', 'tm_block', 'most_read', 'sidebar', '2'); }">Как Тильда убивает рынок веб-фриланса</a>
                                        <div class="post-info__meta">
                  <span class="post-info__meta-item">
                    <svg class="post-info__meta-icon icon-svg_views" width="16" height="9"><use xlink:href="https://habr.com/5efdcb8a/images/common-svg-sprite.svg#eye"></use></svg><span class="post-info__meta-counter post-info__meta-counter_small">59,1k</span>
                  </span>
                                            <a href="#" class="post-info__meta-item" rel="nofollow">
                                                <svg class="post-info__meta-icon icon-svg_comments" width="14" height="13"><use xlink:href="https://habr.com/5efdcb8a/images/common-svg-sprite.svg#comment"></use></svg><span class="post-info__meta-counter post-info__meta-counter_small">170</span>
                                            </a>
                                        </div>
                                    </li>
                                    <li class="content-list__item content-list__item_devided post-info">
                                        <a href="#" class="post-info__title" onclick="if (typeof ga === 'function') { ga('send', 'event', 'tm_block', 'most_read', 'sidebar', '3'); }">США запретили продавать Windows и iPhone российским военным и полиции</a>
                                        <div class="post-info__meta">
                  <span class="post-info__meta-item">
                    <svg class="post-info__meta-icon icon-svg_views" width="16" height="9"><use xlink:href="https://habr.com/5efdcb8a/images/common-svg-sprite.svg#eye"></use></svg><span class="post-info__meta-counter post-info__meta-counter_small">32,1k</span>
                  </span>
                                            <a href="#" class="post-info__meta-item" rel="nofollow">
                                                <svg class="post-info__meta-icon icon-svg_comments" width="14" height="13"><use xlink:href="https://habr.com/5efdcb8a/images/common-svg-sprite.svg#comment"></use></svg><span class="post-info__meta-counter post-info__meta-counter_small">211</span>
                                            </a>
                                        </div>
                                    </li>
                                    <li class="content-list__item content-list__item_devided post-info">
                                        <a href="#" class="post-info__title" onclick="if (typeof ga === 'function') { ga('send', 'event', 'tm_block', 'most_read', 'sidebar', '4'); }">Собеседование наоборот: вопросы соискателя к компании</a>
                                        <div class="post-info__meta">
                  <span class="post-info__meta-item">
                    <svg class="post-info__meta-icon icon-svg_views" width="16" height="9"><use xlink:href="https://habr.com/5efdcb8a/images/common-svg-sprite.svg#eye"></use></svg><span class="post-info__meta-counter post-info__meta-counter_small">22,5k</span>
                  </span>
                                            <a href="#" class="post-info__meta-item" rel="nofollow">
                                                <svg class="post-info__meta-icon icon-svg_comments" width="14" height="13"><use xlink:href="https://habr.com/5efdcb8a/images/common-svg-sprite.svg#comment"></use></svg><span class="post-info__meta-counter post-info__meta-counter_small">74</span>
                                            </a>
                                        </div>
                                    </li>
                                    <li class="content-list__item content-list__item_devided post-info">
                                        <a href="#" class="post-info__title" onclick="if (typeof ga === 'function') { ga('send', 'event', 'tm_block', 'most_read', 'sidebar', '5'); }">Сайты для обучения программированию: Топ 100</a>
                                        <div class="post-info__meta">
                  <span class="post-info__meta-item">
                    <svg class="post-info__meta-icon icon-svg_views" width="16" height="9"><use xlink:href="https://habr.com/5efdcb8a/images/common-svg-sprite.svg#eye"></use></svg><span class="post-info__meta-counter post-info__meta-counter_small">25,6k</span>
                  </span>
                                            <a href="#" class="post-info__meta-item" rel="nofollow">
                                                <svg class="post-info__meta-icon icon-svg_comments" width="14" height="13"><use xlink:href="https://habr.com/5efdcb8a/images/common-svg-sprite.svg#comment"></use></svg><span class="post-info__meta-counter post-info__meta-counter_small">39</span>
                                            </a>
                                        </div>
                                    </li>
                                    <li class="content-list__item content-list__item_devided post-info">
                                        <a href="#" class="post-info__title" onclick="if (typeof ga === 'function') { ga('send', 'event', 'tm_block', 'most_read', 'sidebar', '6'); }">Вероятно, хватит рекомендовать «Чистый код»</a>
                                        <div class="post-info__meta">
                  <span class="post-info__meta-item">
                    <svg class="post-info__meta-icon icon-svg_views" width="16" height="9"><use xlink:href="https://habr.com/5efdcb8a/images/common-svg-sprite.svg#eye"></use></svg><span class="post-info__meta-counter post-info__meta-counter_small">34,8k</span>
                  </span>
                                            <a href="#" class="post-info__meta-item" rel="nofollow">
                                                <svg class="post-info__meta-icon icon-svg_comments" width="14" height="13"><use xlink:href="https://habr.com/5efdcb8a/images/common-svg-sprite.svg#comment"></use></svg><span class="post-info__meta-counter post-info__meta-counter_small">212</span>
                                            </a>
                                        </div>
                                    </li>
                                    <li class="content-list__item content-list__item_devided post-info">
                                        <a href="https://u.tmtm.ru/5servers_rn" class="post-info__title" onclick="if (typeof ga === 'function') { ga('send', 'event', 'tm_block', 'most_read', 'prmlink'); }">Как разгрести зоопарк из 5 размещений в дата-центрах</a>
                                        <div class="post-info__meta-label">
                                            Интересно
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>

                    </div>
                    <script>
                        var rndmvlue = Math.random().toString(36).substring(2);
                        var neuroHabrUrl = '/ru/json/mrnh/?v='+rndmvlue;
                        $.ajax({
                            url: neuroHabrUrl,
                            success: function (data) {
                                if (data.messages === 'ok' && data.html && data.html.length) {
                                    $('#neuro-habr').append(data.html);
                                }
                            },
                            error: function(err) {},
                            dataType: 'json',
                            type: 'GET',
                        });
                    </script>

                </div>
            </div>
        </div>

        <div class="overlay hidden" id="js-vote-result">
            <div class="popup popup_reasons">
                <div class="popup__head">
                    <span class="popup__head-title js-result-popup_title"></span>
                    <button type="button" class="btn btn_small btn_popup-close js-hide_result-reason">
                        <svg class="icon-svg" width="12" height="12">
                            <use xlink:href="https://habr.com/5efdcb8a/images/common-svg-sprite.svg#close"></use>
                        </svg>
                    </button>
                </div>
                <div class="popup__body">
                    <div class="js-reasons-vote_result-empty">Пока никто не указал причину минусов</div>
                    <div class="js-reasons-vote_result-content">
                        <div class="reasons-vote__title">Причины минусов</div>
                        <div id="js-result-popup_list"></div>
                    </div>
                </div>
            </div>
        </div>

    </div>

@endsection
