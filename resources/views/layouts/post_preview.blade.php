<?php
/** @var \App\Models\Post $post */
?>
<li class="content-list__item content-list__item_post shortcuts_item" id="post_{{ $post->id ?? '' }}">

    <article class="post post_preview" lang="ru">
        <header class="post__meta">
            <a href="{{ route('user_page', ['user' => $post->user->nick]) }}"
               class="post__user-info user-info" title="Автор публикации">
                <img
                    src="{{ $post->user->avatar ?? '' }}"
                    width="24" height="24"
                    class="user-info__image-pic user-info__image-pic_small">
                <span
                    class="user-info__nickname user-info__nickname_small">{{ $post->user->nick ?? '' }}</span>
            </a>

{{--            ->format('j F Y \\в h:i')--}}
            <span class="post__time">{{ $post->publicationDateFormatted ?? '' }}</span>
        </header>

        <h2 class="post__title">
            <a href="{{ $post->postUrl ?? '' }}" class="post__title_link">
                {{ $post->title ?? '' }}
            </a>
        </h2>

        @if (isset($post->hubs))
            <ul class="post__hubs post__hubs_full-post inline-list">
                @foreach ($post->hubs as $hub)
                    <li class="inline-list__item inline-list__item_hub">
                        <a href="{{ $hub->hubLink ?? '' }}" class="inline-list__item-link hub-link " title="Вы не подписаны на этот хаб" onclick="if (typeof ga === 'function') { ga('send', 'event', 'hub', 'post page', '{{ $hub->hubName ?? ''}}'); }">{{ $hub->hubName ?? ''}}</a>
                        @if (!$loop->last)
                            ,
                        @endif
                    </li>
                @endforeach
            </ul>
        @endif

        @if (isset($post->marks))
            <ul class="post__marks inline-list">
                @foreach ($post->marks as $mark)
                    <li class="inline-list__item inline-list__item_post-type">
                        @if (isset($mark->markLink))
                            <a href="{{ $mark->markLink ?? '' }}" class="post__type-label" title="{{ $mark->markTitle ?? '' }}">{{ $mark->markLabel ?? '' }}</a>
                        @else
                            <span class="post__type-label" title="{{ $mark->markTitle ?? '' }}">{{ $mark->markLabel ?? '' }}</span>
                        @endif
                    </li>
                @endforeach
            </ul>
        @endif

        <div class="post__body post__body_crop ">

            <div class="post__text post__text-html  post__text_v1 ">
                {!! $post->contentPreview ?? '' !!}
            </div>

            <a class="btn btn_x-large btn_outline_blue post__habracut-btn"
               href="{{ $post->postUrl ?? '' }}">
                Читать дальше &rarr;</a>
        </div>

        <footer class="post__footer">
            <ul class="post-stats  js-user_"
                data-post-type="publish_ugc_ru,h_260,h_477,f_develop"
                id="infopanel_post_{{ $post->id ?? '' }}">
                <li class="post-stats__item">
                    <div class="post-stats__result">

                        <span class="post-stats__result-icon">
{{--                            <svg class="icon-svg_votes" width="10" height="16">--}}
{{--                                <use xlink:href="https://habr.com/5efb4426/images/common-svg-sprite.svg#counter-rating"/>--}}
{{--                            </svg>--}}
                            <span class="icon-svg_votes" style="font-size: 22px; padding: 0 5px;  color: #9dd1e0;">
                                &diams;
                            </span>
                        </span>

                        <span class="post-stats__result-counter voting-wjt__counter_positive "
                              title="Всего голосов {{ $post->votes->plusVotes + $post->votes->minusVotes }}: ↑{{ $post->votes->plusVotes }} и ↓{{ $post->votes->minusVotes }}">
                            @if ($post->rating > 0)
                                +{{ $post->rating }}
                            @else
                                {{ $post->rating }}
                            @endif
                        </span>
                    </div>
                </li>

                <li class="post-stats__item post-stats__item_bookmark">
                    <button type="button" class="btn bookmark-btn bookmark-btn_post "
                            data-post-type="publish_ugc_ru,h_260,h_477,f_develop"
                            data-type="2" data-id="{{ $post->id ?? '' }}" data-action="add"
                            title="Добавить в закладки"
                            onclick="{{-- posts_add_to_favorite(this); --}}">
                            <span class="btn_inner">
                                <svg class="icon-svg_bookmark" width="10" height="16">
                                    <use xlink:href="https://habr.com/5efb4426/images/common-svg-sprite.svg#book"/>
                                </svg>
                                <span class="bookmark__counter js-favs_count"
                                      title="Количество пользователей, добавивших публикацию в закладки">
                                    0
                                </span>
                            </span>
                    </button>
                </li>

                <li class="post-stats__item post-stats__item_views">
                    <div class="post-stats__views" title="Количество просмотров">
                        <svg class="icon-svg_views-count" width="21" height="12">
                            <use xlink:href="https://habr.com/5efb4426/images/common-svg-sprite.svg#eye"/>
                        </svg>
                        <span class="post-stats__views-count">0</span>
                    </div>
                </li>

                <li class="post-stats__item post-stats__item_comments">
                    <a href="#"
                       class="post-stats__comments-link" rel="nofollow">
                        <svg class="icon-svg_post-comments" width="16" height="16">
                            <use
                                xlink:href="https://habr.com/5efb4426/images/common-svg-sprite.svg#comment"/>
                        </svg>
                        <span class="post-stats__comments-count" title="Читать комментарии">0</span>
                    </a>
                </li>

            </ul>
        </footer>
    </article>
</li>
